# Deloitte Standards

## What to expect from this repo?

This repo is meant to guide you over several key points that will help you move faster in your development endeavor and, at the same time, align to the coding standards that Deloitte wants to deliver to their clients.

These repositories are all open for you to file issues and tickets and is the right place to collaborate and bring new knowledge into our workflow and benefit everyone. None of the rules are set on stone. We encourage everyone to start healthy and proactive conversations that can help us move faster and be better at what we do.


## Where to go now

### [Mac Setup](https://bitbucket.org/deloittedigitalcanada/mac-setup/src/master/)

We recommend to start with a basic setup that will help you get your machine up and running all the apps that the team normally use. You'll be ready to code in no time. Run a couple of scripts and you're all done.


### [Code Standards](https://bitbucket.org/deloittedigitalcanada/code-standards/src/master/)

Your next step should be getting to know some of our code standards and how we like keeping the code. In this repo you'll find information about **Gitflow**, **Semantic HTML**, **CSS Architectures and SCSS**, **Accessibility best practices and how to test**, **JavaScript's best practices and how to keep your projects clean and perfomant**, **Testing** and **How to perform code reviews** among other topics. You will be able to find information about frameworks like **React** and how we use them in our client's projects.

### [Accelerators]()
### [Resources]()
We gather all links, tutorials and courses of interest to the community in this section. If you find something that can help the community, please open a ticket and we will try our best to get that info in ASAP.

This is the space where you can have conversations with your peers about the quality of the resources, useful feedback you can provide to your peers and next steps to get them to the next level.

### [Tim's Corner]()